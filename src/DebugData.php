<?php


namespace MfoRu\MfoAccounting\Unicom24;


class DebugData implements \MfoRu\Contracts\MfoAccounting\DebugData
{
    function getRequestHeader():string {
        return '';
    }

    function getRequestBody():string {
        return '';
    }

    function getResponseHeader():string {
        return '';
    }

    function getResponseBody():string {
        return '';
    }
}
