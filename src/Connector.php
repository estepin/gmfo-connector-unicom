<?php
namespace MfoRu\MfoAccounting\Unicom24;


use MfoRu\Contracts\MfoAccounting\Anket;
use MfoRu\Contracts\MfoAccounting\Connector as ConnectorInterface;
use MfoRu\Contracts\MfoAccounting\DebugData;
use MfoRu\Contracts\MfoAccounting\Exceptions\UnknownResponse;

class Connector implements ConnectorInterface
{
    function addToUpload(Anket $anket, $config)
    {
        $api = $this->getApiClient($config);
        $anketArr = $this->anketToApiArr($anket, $api);

        if($id = $api->sendAnket($anketArr)) {
            return $id;
        }
    }

    function getApiClient($config)
    {
        return new Api(['username' => $config->username, 'password' => $config->password]);
    }

    function getAccData($id)
    {
        return false;
    }

    function testConfig($config): bool
    {
        $anket = $this->getTestAnket();
        $api = $this->getApiClient($config);
        $anketArr = $this->anketToApiArr($anket, $api);

        if($api->sendAnket($anketArr)) {
            return true;
        }
        return false;
    }

    function checkStatus($anketId, $config)
    {
        throw new \Exception('This method is not implemented');
    }

    function getTestAnket()
    {
        $anket = new Anket();

        $anket->summ = '1000';
        $anket->term = '5';

        $anket->lastname = 'Петров';
        $anket->firstname = 'Петр';
        $anket->middlename = 'Петрович';
        $anket->gender = '1';
        $anket->birthdate = '1990-01-01';

        $anket->email = 'test@mfo.ru';
        $anket->phone = '+79091112233';

        $anket->passSeriaNum = '11 26 123321';
        $anket->passWhoIssued = 'УВД';
        $anket->passCode = '123-321';
        $anket->passDate = '2011-01-01';
        $anket->passBirthPlace = 'Саратов';

        $anket->regIndex = '410001';
        $anket->regRegion = 'Саратовская обл';
        $anket->regCity = 'Саратов';
        $anket->regStreet = 'Московская';
        $anket->regHouse = '1';
        $anket->regRoom = 1;

        $anket->liveIndex = '410001';
        $anket->liveRegion = 'Саратовская обл';
        $anket->liveCity = 'Саратов';
        $anket->liveStreet = 'Московская';
        $anket->liveHouse = '1';
        $anket->liveRoom = 1;

        return $anket;
    }

    function getConfigModel()
    {
        return new Config();
    }

    function getDebugData(): DebugData
    {
        return new \MfoRu\MfoAccounting\Unicom24\DebugData();
    }

    protected function anketToApiArr(Anket $anket, Api $api)
    {
        $city = preg_replace('!^г !', '', $anket->liveCity);
        $results = $api->findLocationByName($city);

        if(!isset($results[0]['id']) || !$results[0]['id'])
            throw new UnknownResponse('Не известный регион / город');

        $phone = preg_replace('!^\+7|^7!', '', $anket->phone);
        $passport = str_replace(' ', '', $anket->passSeriaNum);
        $passDate = $anket->passDate;
        $locality = $results[0]['id'];

        $arr = [
            'credit_sum' => $anket->summ,
            'loan_type' => 'qiwi',

            'surname' => $anket->lastname,
            'name' => $anket->firstname,
            'patronymic' => $anket->middlename,
            'birth_date' => $anket->birthdate,
            'locality' => $locality,
            'mobile_phone' => $phone,
            'passport' => $passport,
            'passport_date' => $passDate,

            'email' => $anket->email,
            'passport_code' => $anket->passCode,
            'street' => $anket->liveStreet,
            'house' => $anket->liveHouse
        ];

        foreach ($arr as $k => $v)
        {
            if($v === null)
            {
                unset($arr[$k]);
            }
        }

        return $arr;
    }
}
