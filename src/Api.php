<?php


namespace MfoRu\MfoAccounting\Unicom24;


use GuzzleHttp\Client;
use MfoRu\Contracts\MfoAccounting\Exceptions\UnknownResponse;

class Api
{
    protected $authData;
    protected $client;

    function __construct($authData)
    {
        $this->authData = $authData;
        $this->client = new Client([
            'base_uri' => 'https://unicom24.ru/api/partners/requests/v1/',
            'timeout' => 30
        ]);
    }

    function sendAnket($anketFields)
    {

        $response = $this->client->post(
            'create_send_all/',
            [
                'http_errors' => false,
                'auth' => [$this->authData['username'], $this->authData['password']],
                'json' => $anketFields
            ]
        );

        if($response->getStatusCode() == 201) {
            $content = $response->getBody()->getContents();
            $data = json_decode($content, true);
            if(isset($data['id']))
                return $data['id'];

            throw new UnknownResponse();
        } elseif($response->getStatusCode() == 400) {
            throw new UnknownResponse();
        }

        throw new \Exception();
    }


    function findLocationByName($name)
    {
        $url = '​https://unicom24.ru/api/partners/requests/v1/locality/';
        $response = $this->client->get(
            'locality/?term='.$name,
            [
                'auth' => [$this->authData['username'], $this->authData['password']]
            ]
        );

        $content = $response->getBody()->getContents();
        return json_decode($content, true);
    }

    function listRegions()
    {
        $url = 'region/';
        $response = $this->client->get(
            $url,
            [
                'auth' => [$this->authData['username'], $this->authData['password']]
            ]
        );
        $content = $response->getBody()->getContents();
        if($content && $resData = json_decode($content))
            return $resData;
    }

}
